PKG=ROI.models.netlib
EMAIL="FlorianSchwendinger@gmx.at"
R ?= R


build:
	$(R) CMD build .

inst: build
	$(R) CMD INSTALL ${PKG}_*.tar.gz
	
check: build
	$(R) CMD check ${PKG}_*.tar.gz

check_as_cran: build
	$(R) CMD check --as-cran ${PKG}_*.tar.gz



manual: clean
	$(R) CMD Rd2pdf --output=Manual.pdf .

clean:
	rm -f Manual.pdf README.knit.md README.html
	rm -rf .Rd2pdf*

check_all: build
	$(R) -e "rhub::check(dir(pattern = '${PKG}_.*.tar.gz'), platform = c('macos-highsierra-release-cran', 'debian-clang-devel', 'windows-x86_64-devel', 'fedora-clang-devel'))"

check_fedora: build
	$(R) -e "rhub::check(dir(pattern = '${PKG}_.*.tar.gz'), platform = 'fedora-clang-devel')"

check_win: build
	$(R) -e "rhub::check(dir(pattern = '${PKG}_.*.tar.gz'), platform = 'windows-x86_64-devel')"

check_old_win: build
	$(R) -e "rhub::check(dir(pattern = '${PKG}_.*.tar.gz'), platform = 'windows-x86_64-devel')"

devcheck_win_devel: build
	$(R) -e "devtools::check_win_devel(email = '${EMAIL}')"

devcheck_win_release: build
	$(R) -e "devtools::check_win_release(email = '${EMAIL}')"

devcheck_win_oldrelease: build
	$(R) -e "devtools::check_win_oldrelease(email = '${EMAIL}')"

check_mac_m1: build
	$(R) -e "rhub::check(dir(pattern = '${PKG}_.*.tar.gz'), platform = 'macos-m1-bigsur-release')"

check_mac_old: build
	$(R) -e "rhub::check(dir(pattern = '${PKG}_.*.tar.gz'), platform = 'macos-highsierra-release-cran')"

check_linux_san: build
	$(R) -e "rhub::check(dir(pattern = '${PKG}_.*.tar.gz'), platform = 'linux-x86_64-rocker-gcc-san')"
